export declare type OnItemProcessed<T> = (item: T) => Promise<void>;

declare interface RunProgress
{
	done: number,
}


export function runInParallel<T>(concurrency: number, data: Array<T>, onItem: OnItemProcessed<T>): Promise<void>
{
	data = [...data];

	const total = data.length;

	if (!total) {
		return;
	}

	return new Promise((resolve, reject) => {
		const threads = Math.min(concurrency, total);
		const progress: RunProgress = {
			done: 0,
		};

		for (let i = 0; i < threads; i++) {
			processNext(data, onItem, total, progress, resolve, reject);
		}
	});
}


function processNext<T>(data: Array<T>, processItem: OnItemProcessed<T>, total: number, progress: RunProgress, resolve: () => void, reject: (err: Error) => void): void
{
	if (!data.length) {
		return;
	}

	const item = data.shift();

	processItem(item)
		.then(() => {
			progress.done++;

			if (progress.done === total) {
				return resolve();
			}

			processNext(data, processItem, total, progress, resolve, reject);
		})
		.catch((err) => {
			reject(err);
		});
}
