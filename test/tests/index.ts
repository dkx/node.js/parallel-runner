import {expect, use as chaiUse} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import {runInParallel} from '../../lib';


chaiUse(chaiAsPromised);


describe('runInParallel()', () => {

	it('should run for empty data', async () => {
		await runInParallel(1, [], async () => {});
	});

	it('should run in series', async () => {
		const data = [];

		await runInParallel(1, [1, 2, 3, 4, 5], async (item) => {
			data.push(item);
		});

		expect(data).to.be.eql([1, 2, 3, 4, 5]);
	});

	it('should stop processing on error', async () => {
		const data = [];

		await expect(runInParallel(1, [1, 2, 3, 4, 5], async (item) => {
			if (item === 3) {
				throw new Error('Three is bad');
			}

			data.push(item);
		})).to.be.rejectedWith(Error, 'Three is bad');

		expect(data).to.be.eql([1, 2]);
	});

	it('should run in parallel', async () => {
		const data = [];

		await runInParallel(5, [1, 2, 3, 4, 5], async (item) => {
			await waitForRandomTime(0, 100);
			data.push(item);
		});

		expect(data).to.have.members([1, 2, 3, 4, 5]);
	});

});


function waitForRandomTime(from: number, to: number): Promise<void>
{
	const time = Math.floor(Math.random() * to) + from;
	return new Promise((resolve) => {
		setTimeout(resolve, time);
	})
}
