# DKX/ParallelRunner

Simple promise parallel runner for node.js.

## Installation

```bash
$ npm install --save @dkx/parallel-runner
```

or with yarn

```bash
$ yarn add @dkx/parallel-runner
```

## Usage

```typescript
import {runInParallel} from '@dkx/parallel-runner';

(async () => {
	await runInParallel(2, [1, 2, 3, 4, 5], async (item: number) => {
		await doSomethingWithItem(item);
	});
	
	console.log('all items were processed in 2 threads');
})();
```
